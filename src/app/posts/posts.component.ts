import { Component, OnInit } from '@angular/core';
//import { PostsService } from './../posts.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Post} from '../post';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  readonly ROOT_URL = 'https://jsonplaceholder.typicode.com';

  posts: Observable<Post[]>;
 
 constructor(private http: HttpClient){}
 
   getPosts(){
     this.posts = this.http.get<Post[]>(this.ROOT_URL + '/posts')
  
   }
   ngOnInit() {}
 
  
  /*
title;


body;
postsData$:Observable<Posts>;

  constructor(private route: ActivatedRoute, private postsService: PostsService) { }
 
  ngOnInit() {
   // this.title= this.route.snapshot.params.title;
   // this.body= this.route.snapshot.params.body;
    this.postsData$ = this.postsService.getPosts(this.title);
    this.postsData$.subscribe(
      data=>{
        console.log(data);
        this.title = data.title;
        this.body = data.body;
      }
    )
   
  }
*/}
